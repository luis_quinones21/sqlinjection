import mysql.connector
from random import randint

try:
    db = mysql.connector.connect(host="localhost", user="root", passwd="python1234", db="sqlinjection")
    cursor = db.cursor()

    delTable = "DROP TABLE IF EXISTS phonebook;"

    cursor.execute(delTable)

    creTable = "CREATE TABLE phonebook (name VARCHAR(20) NOT NULL, phone VARCHAR(12) NOT NULL);"

    cursor.execute(creTable)

    entries = ["Alan", "Ada", "Donald", "Grace", "Claude", "Ken", "Dennis", "Tim", "Tony", "Vint"]

    for i in range(len(entries)):
        randphone = str(randint(100, 999)) + "-" + str(randint(100, 999)) + "-" + str(randint(1000, 9999))
        insDB = "insert into phonebook (name, phone) VALUES ('"

        insDB += entries[i]

        insDB += "' , '"

        insDB += randphone

        insDB += "');"

        cursor.execute(insDB)

    db.commit()
    cursor.close()
    db.close()
except Exception as e:
    print e