import mysql.connector
from random import randint
import hashlib

try:
    db = mysql.connector.connect(host="localhost",
                                 user="root",
                                 passwd="python1234",
                                 db="sqlinjection")

    cursor = db.cursor()

    delTable = "DROP TABLE IF EXISTS login;"

    cursor.execute(delTable)

    creTable = "CREATE TABLE login (username VARCHAR(20) NOT NULL, password VARCHAR(32) NOT NULL);"

    cursor.execute(creTable)

    # Crear una tabla de cuentas de usuarios con sus hashes de manera aleatoria con 50 usuarios
    for t in range(50):

        largoUser = randint(8, 16)
        username = ""
        for i in range(largoUser):
            randChar = randint(48, 126)
            if randChar == 92:
                randChar += 1
            username += str(unichr(randChar))

        largoPass = randint(8, 20)
        password = ""
        for j in range(largoUser):
            randChar = randint(48, 126)
            if randChar == 92:
                randChar += 1
            password += str(unichr(randChar))

        password = hashlib.md5(password).hexdigest()

        insDB = "insert into login (username, password) VALUES ('"

        insDB += username

        insDB += "' , '"

        insDB += password

        insDB += "');"


        cursor.execute(insDB)

    insUser = "insert into login (username, password) VALUES ('dbadmin','64387b4b69baef645c388a6c3b812c52');"

    cursor.execute(insUser)

    db.commit()

    cursor.close()
    db.close()
except Exception as e:
    print e
